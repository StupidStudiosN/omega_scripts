local m=120223103
local cm=_G["c"..m]
cm.name="火焰支配者"
function cm.initial_effect(c)
	--Double Tribute
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_DOUBLE_TRIBUTE)
	e1:SetValue(cm.trival)
	c:RegisterEffect(e1)
end
--Double Tribute
cm.trival=RD.ValueDoubleTributeAttrRace(ATTRIBUTE_FIRE,nil)