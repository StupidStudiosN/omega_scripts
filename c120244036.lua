local m=120244036
local list={120222017,120213023}
local cm=_G["c"..m]
cm.name="蛋球机器爆速龙"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
	--Atk Up
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetCategory(CATEGORY_ATKCHANGE)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(cm.cost)
	e1:SetOperation(cm.operation)
	c:RegisterEffect(e1)
end
--Atk Up
function cm.costfilter(c)
	return c:IsCode(list[2])
end
function cm.desfilter(c)
	return c:IsFaceup() and c:IsAttackAbove(1500)
end
function cm.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	local ct=Duel.GetMatchingGroupCount(cm.costfilter,tp,LOCATION_GRAVE,LOCATION_GRAVE,nil)
	if chk==0 then return Duel.IsPlayerCanDiscardDeckAsCost(tp,ct) end
	if Duel.DiscardDeck(tp,ct,REASON_COST)~=0 then
		local og=Duel.GetOperatedGroup()
		if og:IsExists(Card.IsAttackBelow,1,nil,1500) then
			e:SetLabel(m)
		else
			e:SetLabel(0)
		end
	end
end
function cm.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsFaceup() and c:IsRelateToEffect(e) then
		RD.AttachAtkDef(e,c,1000,0,RESET_EVENT+RESETS_STANDARD+RESET_DISABLE+RESET_PHASE+PHASE_END)
		if e:GetLabel()==m then
			RD.CanSelectAndDoAction(aux.Stringid(m,1),HINTMSG_DESTROY,cm.desfilter,tp,0,LOCATION_MZONE,1,1,nil,function(g)
				Duel.Destroy(g,REASON_EFFECT)
			end)
		end
	end
end