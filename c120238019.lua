local m=120238019
local cm=_G["c"..m]
cm.name="伟大魔兽 加泽特"
function cm.initial_effect(c)
	--Atk Up
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_MATERIAL_CHECK)
	e1:SetValue(cm.check)
	c:RegisterEffect(e1)
	--Summon Only
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_SUMMON_COST)
	e2:SetOperation(cm.facechk)
	e2:SetLabelObject(e1)
	c:RegisterEffect(e2)
end
--Atk Up
function cm.check(e,c)
	local tc=c:GetMaterial():GetFirst()
	local atk=0
	if tc then atk=RD.GetBaseAttackOnTribute(tc)*2 end
	if atk>0 and e:GetLabel()==1 then
		e:SetLabel(0)
		RD.AttachAtkDef(e,c,atk,0,RESET_EVENT+0xff0000)
	end
end
--Summon Only
function cm.facechk(e,tp,eg,ep,ev,re,r,rp)
	e:GetLabelObject():SetLabel(1)
end